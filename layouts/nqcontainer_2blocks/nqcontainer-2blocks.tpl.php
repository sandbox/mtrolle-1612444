<?php
/**
 * @file
 * Template for nqcontainers layout
 *
 * Variables:
 * - $positions: An array of content for each positions (numeric, position 1 should use $positions[1] etc.
 * - $container An array of settings for the container currently using this theme
 */
?>
<div class="node-container nqcontainer_2blocks-parent">
	
	<div class="nqcontainer_2blocks left position-1">
		<?php print $positions[1]; ?>
	</div>
	<div class="nqcontainer_2blocks right position-2">
		<?php print $positions[2]; ?>
	</div>


	<div style="clear:both; height:10px;">&shy;</div>
</div>