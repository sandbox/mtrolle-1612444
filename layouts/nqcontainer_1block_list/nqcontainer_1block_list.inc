<?php

// Plugin definition
$plugin = array(
  'title' => t('1 block + list'),
  'category' => '1 cols',
  'icon' => 'nqcontainer_1block_list.png',
  'theme' => 'nqcontainer_1block_list',
  'css' => 'nqcontainer_1block_list.css',
  'positions' => array(
    1 => array(), // its safe to leave an empty array as this will default to all available fields
    2 => array('title'), // we only want title here
    3 => array('title'), // we only want title here
    4 => array('title'), // we only want title here
  ),
);
