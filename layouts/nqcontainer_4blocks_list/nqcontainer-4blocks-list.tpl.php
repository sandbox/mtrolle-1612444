<?php
/**
 * @file
 * Template for mtcontainers layout
 *
 * Variables:
 * - $positions: An array of content for each positions (numeric, position 1 should use $positions[1] etc.
 * - $container An array of settings for the container currently using this theme
 */
?>
<div class="node-container nqcontainer_4blocks_list">
	
	<div class="nqcontainer_4blocks_list-main left">
	
		<!-- Top cols left -->
		<div class="nqcontainer_4blocks_list-left-top left position-1">
			<?php print $positions[1]; ?>
		</div>
		
		<div class="nqcontainer_4blocks_list-left-top right position-2">
			<?php print $positions[2]; ?>
		</div>
		
		<div style="clear:both; height:10px;">&shy;</div>
		
		
		<!-- Bottom cols left -->
		<div class="nqcontainer_4blocks_list-left-bottom positions-5-8">
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-5">
				<?php print $positions[5]; ?>
			</div>
			
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-6">
				<?php print $positions[6]; ?>
			</div>
			
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-7">
				<?php print $positions[7]; ?>
			</div>
			
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-8">
				<?php print $positions[8]; ?>
			</div>
		</div>
	</div>
	
	<div class="nqcontainer_4blocks_list-main right positions-3-to-4">
		<div class="position-3">
			<?php print $positions[3]; ?>
		</div>
		
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-4">
			<?php print $positions[4]; ?>
		</div>
	</div>
	
</div>