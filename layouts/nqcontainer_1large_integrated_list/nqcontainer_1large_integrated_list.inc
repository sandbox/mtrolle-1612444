<?php

// Plugin definition
$plugin = array(
  'title' => t('1 large with integrated list for breakings'),
  'category' => '1 cols',
  'icon' => 'nqcontainer_1large_integrated_list.png',
  'theme' => 'nqcontainer_1large_integrated_list',
  'css' => 'nqcontainer_1large_integrated_list.css',
  'positions' => array(
    1 => 'node', // We can provide string 'node' to just get the actual node object
    2 => array('title'), // we only want title here
    3 => array('title'), // we only want title here
    4 => array('title'), // we only want title here
    5 => array('title'), // we only want title here
    6 => array('title'), // we only want title here
    7 => array('title'), // we only want title here
  ),
);
