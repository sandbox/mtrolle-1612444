<?php
/**
 * @file
 * Template for nqcontainers layout
 *
 * Variables:
 * - $positions: An array of content for each positions (numeric, position 1 should use $positions[1] etc.
 * - $container An array of settings for the container currently using this theme
 */
?>
<div class="node-container nqcontainer-2-3-cols">
	
	<div class="nqcontainer-2-3-cols-main left">
	
		<!-- Top col left -->
		<div class="nqcontainer-2-3-cols-top position-1">
			<?php print $positions[1]; ?>
		</div>
		
		<div style="clear:both; height:10px;">&shy;</div>
		
		<!-- Bottom cols left -->
		<div class="nqcontainer-2-3-cols-bottom left positions-3-to-5">
		
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-3">
				<?php print $positions[3]; ?>
			</div>
		
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-4">
				<?php print $positions[4]; ?>
			</div>
		
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-5">
				<?php print $positions[5]; ?>
			</div>
		</div>
		
		<div class="nqcontainer-2-3-cols-bottom right positions-10-to-15">
		
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-2">
				<?php print $positions[2]; ?>
			</div>
			
			
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-10">
				<?php print $positions[10]; ?>
			</div>
		
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-11">
				<?php print $positions[11]; ?>
			</div>
		
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-12">
				<?php print $positions[12]; ?>
			</div>
		
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-13">
				<?php print $positions[13]; ?>
			</div>
		</div>
		
	</div>
	
	<div class="nqcontainer-2-3-cols-main right positions-6-to-9">
			<div class="position-6">
				<?php print $positions[6]; ?>
			</div>
		
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-7">
				<?php print $positions[7]; ?>
			</div>
		
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-8">
				<?php print $positions[8]; ?>
			</div>
		
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-9">
				<?php print $positions[9]; ?>
			</div>
		
		
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-14">
				<?php print $positions[14]; ?>
			</div>
			
			<div class="nqcontainer-hr">&nbsp;</div>
			<div class="position-15">
				<?php print $positions[15]; ?>
			</div>
	</div>
	<div style="clear:both; height:10px;">&shy;</div>
</div>