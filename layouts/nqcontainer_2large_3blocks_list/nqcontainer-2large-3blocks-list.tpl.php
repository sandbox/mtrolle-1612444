<?php
/**
 * @file
 * Template for nqcontainers layout
 *
 * Variables:
 * - $positions: An array of content for each positions (numeric, position 1 should use $positions[1] etc.
 * - $container An array of settings for the container currently using this theme
 */
?>
<div class="node-container nqcontainer-2large-3blocks-list">
	<!-- Top col -->
	<div class="nqcontainer-2large-3blocks-list-top position-1">
		<?php print $positions[1]; ?>
	</div>
	
	<div style="clear:both; height:10px;">&shy;</div>
	
	<!-- Bottom cols -->
	<div class="nqcontainer-2large-3blocks-list-bottom left positions-3-to-5">
	
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-3">
			<?php print $positions[3]; ?>
		</div>
	
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-4">
			<?php print $positions[4]; ?>
		</div>
	
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-5">
			<?php print $positions[5]; ?>
		</div>
	</div>
	
	<div class="nqcontainer-2large-3blocks-list-bottom right positions-2-and-6-to-9">
	
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-2">
			<?php print $positions[2]; ?>
		</div>
		
		
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-6">
			<?php print $positions[6]; ?>
		</div>
	
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-7">
			<?php print $positions[7]; ?>
		</div>
	
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-8">
			<?php print $positions[8]; ?>
		</div>
	
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-9">
			<?php print $positions[9]; ?>
		</div>
	</div>
		
	<div style="clear:both; height:10px;">&shy;</div>
</div>