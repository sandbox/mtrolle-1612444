jQuery(document).ready(function () {
	// Blur function on nodequeue name change to set container name
  jQuery('#edit-sqid').blur(function() {
      queue_name = jQuery('#edit-sqid').val();
      name = queue_name.replace(/\d+-/, '');

      jQuery('#edit-name').val(name).blur();
  });
  
  // Blue function on container name to set machine name
  jQuery('#edit-name').blur(function() {
      name = jQuery('#edit-name').val()
      machine_name = name.replace(/[^\w\s]+/g, '').replace(/\s+/g, '_').toLowerCase();

			if(jQuery('#edit-machine-name').attr('disabled') !== true) {
	      jQuery('#edit-machine-name').val(machine_name);
	    }
  });
});