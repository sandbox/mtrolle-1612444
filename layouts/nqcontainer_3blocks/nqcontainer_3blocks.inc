<?php

// Plugin definition
$plugin = array(
  'title' => t('3 blocks'),
  'category' => '3 cols',
  'icon' => 'nqcontainer_3blocks.png',
  'theme' => 'nqcontainer_3blocks',
  'css' => 'nqcontainer_3blocks.css',
  'positions' => array(
    1 => array(), // its safe to leave an empty array as this will default to all available fields
    2 => array(), // its safe to leave an empty array as this will default to all available fields
    3 => array(), // its safe to leave an empty array as this will default to all available fields
  ),
);
