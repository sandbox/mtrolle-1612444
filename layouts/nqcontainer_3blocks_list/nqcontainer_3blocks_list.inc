<?php

// Plugin definition
$plugin = array(
  'title' => t('3 blocks + list'),
  'category' => '2 cols',
  'icon' => 'nqcontainer_3blocks_list.png',
  'theme' => 'nqcontainer_3blocks_list',
  'css' => 'nqcontainer_3blocks_list.css',
  'positions' => array(
    1 => array(), // its safe to leave an empty array as this will default to all available fields
    2 => array(), // its safe to leave an empty array as this will default to all available fields
    3 => array(), // its safe to leave an empty array as this will default to all available fields
    4 => array('title', 'teaser'), // we only want title here
    5 => array('title', 'teaser'), // we only want title here
    6 => array('title', 'teaser'), // we only want title here
    7 => array('title'), // we only want title here
    8 => array('title'), // we only want title here
  ),
);
