<?php
/**
 * @file
 * Template for nqcontainers layout
 *
 * Variables:
 * - $positions: An array of content for each positions (numeric, position 1 should use $positions[1] etc.
 * - $container An array of settings for the container currently using this theme
 */
 
$main_story = $positions[1];
if($main_story):
	/**
	 * We'll do some formatting and parsing to prepare our data for the template
	 */
	 
	// Parsing main story body
	$body = field_get_items('node', $main_story, 'body');
	if($body):
		$body = field_view_value('node', $main_story, 'body', $body[0], array(
		  'type' => 'text_summary_or_trimmed',
		  'settings' => array(
		    'trim_length' => 200,
		  ),
		));
		$main_story_body = render($body);
	endif;
	
	// Parsing main story image
	$image = field_get_items('node', $main_story, 'field_image');
	if($image):
		$image = field_view_value('node', $main_story, 'field_image', $image[0], array(
		  'type' => 'image',
		  'settings' => array(
		    'image_style' => 'nodequeue_containers-crop-horizontal-large',
		    'image_link' => 'content',
		  ),
		));
		
		$image_tag = render($image);
		preg_match('@src="(.*?)"@', $image_tag, $image);
		
		$main_story_image = $image[1];
	endif;

?>
<div class="node-container nqcontainer_1large_integrated_list">

	<div style="background-image:url(<?=isset($main_story_image)? $main_story_image : ''; ?>);" class="nqcontainer_1large_integrated_list main-container">
		<div class="nqcontainer_main_story">
			<h3><?=l($main_story->title, 'node/'. $main_story->nid); ?></h3>
			<?=isset($main_story_body)? l(strip_tags($main_story_body), 'node/'. $main_story->nid) : ''; ?>
		</div>
	
		<div class="nqcontainer_1large_integrated_list list positions-2-to-6">
			<div class="position-2">
				<?php print $positions[2]; ?>
			</div>
			<div class="position-3">
				<?php print $positions[3]; ?>
			</div>
			<div class="position-4">
				<?php print $positions[4]; ?>
			</div>
			<div class="position-5">
				<?php print $positions[5]; ?>
			</div>
			<div class="position-6">
				<?php print $positions[6]; ?>
			</div>
		</div>
	</div>
</div>

<?php
endif; // of if($main_story)
?>