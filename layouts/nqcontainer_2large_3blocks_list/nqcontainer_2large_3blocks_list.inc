<?php

// Plugin definition
$plugin = array(
  'title' => t('2 large, 3 blocks & list'),
  'category' => '2 cols',
  'icon' => 'nqcontainer_2large_3blocks_list.png',
  'theme' => 'nqcontainer_2large_3blocks_list',
  'css' => 'nqcontainer_2large_3blocks_list.css',
  'positions' => array(
    1 => array('image-large', 'title', 'teaser', 'related'), // its safe to leave an empty array as this will default to all available fields
    2 => array(), // its safe to leave an empty array as this will default to all available fields
    3 => array('image', 'title'), // its safe to leave an empty array as this will default to all available fields
    4 => array('image', 'title'), // its safe to leave an empty array as this will default to all available fields
    5 => array('image', 'title'), // its safe to leave an empty array as this will default to all available fields
    6 => array('title'), // its safe to leave an empty array as this will default to all available fields
    7 => array('title'), // its safe to leave an empty array as this will default to all available fields
    8 => array('title'), // its safe to leave an empty array as this will default to all available fields
    9 => array('title'), // its safe to leave an empty array as this will default to all available fields
  ),
);
