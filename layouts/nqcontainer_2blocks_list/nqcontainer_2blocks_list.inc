<?php

// Plugin definition
$plugin = array(
  'title' => t('2 blocks and a list'),
  'category' => '2 cols',
  'icon' => 'nqcontainer_2blocks_list.png',
  'theme' => 'nqcontainer_2blocks_list',
  'css' => 'nqcontainer_2blocks_list.css',
  'positions' => array(
    1 => array(), // its safe to leave an empty array as this will default to all available fields
    2 => array(), // its safe to leave an empty array as this will default to all available fields
    3 => array('title', 'teaser'), // we only want teaser and titles here
    4 => array('title', 'teaser'), // we only want teaser and titles here
    5 => array('title'), // we only want title here
    6 => array('title'), // we only want title here
  ),
);
