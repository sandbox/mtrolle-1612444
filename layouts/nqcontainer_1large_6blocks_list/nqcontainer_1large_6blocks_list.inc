<?php

// Plugin definition
$plugin = array(
  'title' => t('1 large, 6 blocks & list'),
  'category' => '3 cols',
  'icon' => 'nqcontainer_1large_6blocks_list.png',
  'theme' => 'nqcontainer_1large_6blocks_list',
  'css' => 'nqcontainer_1large_6blocks_list.css',
  'positions' => array(
    1 => array(), // its safe to leave an empty array as this will default to all available fields
    2 => array('image', 'title'),
    3 => array('image', 'title'),
    4 => array('image', 'title'),
    5 => array('image', 'title'),
    6 => array('title'),
    7 => array('title'),
    8 => array('title'),
    9 => array('title'),
    10 => array('title'),
  ),
);
