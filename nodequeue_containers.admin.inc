<?php
/**
 * @file
 * Admin functions for mt containers module
 */

/** 
 * Admin page for nodequeue_containers module, listing all created containers
 */
function nodequeue_containers_admin() {
	// Creates table of listing types
	$headers = array(t('NAME'), t('SUBQUEUE'), t('DESCRIPTION'), t('OPERATIONS'));
	$rows = array();
	
	// finds the actual listing types in mysql
	$sth = db_query("SELECT * FROM {nodequeue_containers} ORDER BY `name`");
	foreach($sth as $row) {
		$queue = subqueue_load($row->sqid);
		$rows[] = array(
			l($row->name, 'admin/structure/nodequeue/'. $row->qid .'/view/'. $row->sqid),
			$queue->title,
			$row->description,
			l(t('view'), 'admin/structure/nodequeue/'. $row->qid .'/view/'. $row->sqid) .' &nbsp; '. l(t('change layout'), 'admin/structure/nodequeue/'. $row->qid .'/view/'. $row->sqid .'/container-layout') .' &nbsp; '. l(t('edit'), 'admin/structure/nodequeue/'. $row->qid .'/view/'. $row->sqid .'/container-settings') .' &nbsp; '. l(t('delete'), 'admin/structure/nodequeue/'. $row->qid .'/view/'. $row->sqid .'/container-delete'),
		);
	}
	
	// builds talbe
	$table = theme('table', array('header' => $headers, 'rows' => $rows));
	
	return $table;
}


/**
 * Menu callback: Form to add and edit nodequeue_containers
 * NOTICE Edit id should be id of nodequeue - not the container id
 *
 * @return array
 */
function nodequeue_containers_form_edit($form, &$form_state, $edit_id=0) {
	if(!is_numeric($edit_id) && !empty($edit_id)) {
		drupal_set_message('Expecting edit_id to be numeric or empty - "<em>'. $edit_id .'</em>" given', 'error');
	}
	
	if($edit_id > 0) {
		$row = nodequeue_containers_load_from_subqueue_id($edit_id);
		if(!$row) {
			drupal_set_message('There has not yet been created any node container for this subqueue. Complete this form to create it');
			
			$queue = subqueue_load($edit_id);
			// Calculates a machine name based on subqueue title
			$machine_name = preg_replace('@[^\w\s]@', '', $queue->title);
			$machine_name = preg_replace('@\s@', '_', $machine_name);
			$machine_name = strtolower($machine_name);
			$row = (object)array('name' => $queue->title, 'machine_name' => $machine_name, 'description' => '', 'sqid' => $queue->sqid .'-'. $queue->title, 'cid' => 0, 'layout' => '');
		} else {
			// We'll load subqueue to provide human readable value in nodequeue field
			$queue = subqueue_load($row->sqid);
			$row->sqid = $queue->sqid .'-'. $queue->title;
		}
	} else {
		$row = (object)array('name' => '', 'machine_name' => '', 'description' => '', 'sqid' => '', 'cid' => 0, 'layout' => '');
	}
	
	// loads js file which helps us with UI of autocomplete
	drupal_add_js(drupal_get_path('module', 'nodequeue_containers') . '/nodequeue_containers.js');
	
	$form['cid'] = array(
		'#type' => 'hidden',
		'#default_value' => $row->cid,
	);
	
	
	$form['sqid'] = array(
    '#type' => 'textfield',
    '#title' => t('Nodequeue'),
    '#default_value' => $row->sqid,
    '#description' => t('The nodequeue subqueue this container should be based on.'),
    '#required' => TRUE,
    '#autocomplete_path' => 'admin/structure/nodequeue/containers/autocomplete-subqueues',
	);
	
	$form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $row->name,
    '#description' => t('Administration name of the node container.'),
    '#required' => TRUE,
	);
	
	$form['machine_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine name'),
    '#default_value' => $row->machine_name,
    '#description' => t('Machine name of the container.'),
    '#required' => TRUE,
	);
	
	// If machine name isn't empty, we'll not allow override.
	if(!empty($row->machine_name)) {
		$form['machine_name']['#disabled'] = TRUE;
	}
  	
	$form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $row->description,
    '#description' => t('Administration description of the node container.'),
    '#required' => FALSE,
	);
	
	
	// Fetches layout field
	$form['layout'] = _nodequeue_containers_build_form_layout_field($row->layout);
  
  	
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
	);
  		
  return $form;
}

/**
 * Returns a form field element for layout selection
 */
function _nodequeue_containers_build_form_layout_field($chosen_layout) {
	// Include CSS to make layouts be presented on one line
	drupal_add_css(drupal_get_path('module', 'nodequeue_containers') . '/nodequeue_containers.css');
	
	// Builds an array of layout options
	$layout_options = array();
	foreach(nodequeue_containers_get_layouts() as $layout):
		$layout_options[$layout['name']] = '<div class="layout-icon"><img src="'. $GLOBALS['base_url'] .'/'. $layout['path'] .'/'. $layout['icon'] .'"><br>'. $layout['title'] .'<br>(<em><small>'. t('!i positions', array('!i' => count($layout['positions']))) .'</small></em>)</div>';
	endforeach;
	
	// creates form element
	return array(
		'#type' => 'radios',
		'#title' => t('Choose layout'),
		'#default_value' => $chosen_layout,
		'#required' => TRUE,
		'#options' => $layout_options,
	);
}

/**
 * Validate handler for nodequeue_containers_form_edit()
 */
function nodequeue_containers_form_edit_validate(&$form, &$state) {
	// Subqueue id is shown human friendly like id-name - we'll parse it so we only have id
  preg_match('@(\d+)-@', $state['values']['sqid'], $matches);
  $sqid = $matches[1];
  
  // And now validate, that there doesn't exists another container for this queue already, as we can only have one container per queue
	$sth = db_select('nodequeue_containers')
				 ->fields('nodequeue_containers')
				 ->condition('sqid', $sqid, '=')
				 ->condition('cid', $state['values']['cid'], '!=')
				 ->execute();
	
	if($sth->rowCount() > 0) {
		form_set_error('sqid', t('There already exists another node container on the selected subqueue. You can only have one node container per subqueue.'));
	}
	elseif(!subqueue_load($sqid)) {
		form_set_error('sqid', t('You have tried to create a node container on a subqueue that doesn\'t exists'));
	}
	
	// Also we have to validate that same machine name isn't used on another container
	$sth = db_select('nodequeue_containers')
				 ->fields('nodequeue_containers')
				 ->condition('machine_name', $state['values']['machine_name'], '=')
				 ->condition('cid', $state['values']['cid'], '!=')
				 ->execute();
	
	if($sth->rowCount() > 0) {
		form_set_error('machine_name', t('There already exists another node container with same machine name - machine name has to be unique.'));
	}
}

/**
 * Submit handler for nodequeue_containers_form_edit()
 */
function nodequeue_containers_form_edit_submit(&$form, &$state) {
	// First we need to know if this is an update or an insert
	if ($state['values']['cid'] == 0) {
		$sth = db_insert('nodequeue_containers');
	}
	else {
		$sth = db_update('nodequeue_containers')
		->condition('cid', $state['values']['cid'], '=');
	}
	
	// Subqueue id is shown human friendly like id-name - we'll parse it so we only have id
  preg_match('@(\d+)-@', $state['values']['sqid'], $matches);
  $sqid = $matches[1];
  // we load the subqueue to have its nodequeue id as well
  $queue = subqueue_load($sqid);
	
	$insert_id = $sth->fields(array(
		'sqid' => $sqid,
		'qid' => $queue->qid,
		'name' => $state['values']['name'],
		'machine_name' => $state['values']['machine_name'],
		'description' => $state['values']['description'],
		'layout' => $state['values']['layout'],
	))
	->execute();
	
	drupal_set_message(t('The node container @name is now saved.', array('@name' => $state['values']['name'])));
	drupal_goto('admin/structure/nodequeue/'. $queue->qid .'/view/'. $queue->sqid);
}


/**
 * Menu callback -- ask for confirmation of container deletion
 */
function nodequeue_containers_form_delete($form, &$form_state, $sqid) {
  // Always provide entity id in the same form key as in the entity edit form.
  $container = nodequeue_containers_load_from_subqueue_id($sqid);
  $form['cid'] = array('#type' => 'value', '#value' => $container->cid);
  return confirm_form($form,
    t('Are you sure you want to delete the node container %name?', array('%name' => $container->name)),
    'admin/structure/nodequeue/'. $container->qid .'/view'. $container->sqid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute node deletion
 */
function nodequeue_containers_form_delete_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $container = nodequeue_containers_load($form_state['values']['cid']);
    
    // Deletes the actual listing type
    db_delete('nodequeue_containers')
    ->condition('cid', $container->cid, '=')
    ->execute();
    
    drupal_set_message(t('The node container %name has been deleted.', array('%name' => $container->name)));
  }

  $form_state['redirect'] = 'admin/structure/nodequeue/containers';
}

/**
 * Menu callback: Form to edit layout of given nodequeue_containers
 * NOTICE Edit id should be id of a subqueue - not the container id
 *
 * @return array
 */
function nodequeue_containers_form_layout($form, &$form_state, $edit_id=0) {
	if(!is_numeric($edit_id) && !empty($edit_id)) {
		drupal_set_message('Expecting edit_id to be numeric or empty - "<em>'. $edit_id .'</em>" given', 'error');
	}
	
	
	$row = nodequeue_containers_load_from_subqueue_id($edit_id);
	if(!$row) {
		drupal_set_message('There has not yet been created any node container for this subqueue. You need to create a container before you can set its design.', 'warning');
		return $form['error'] = array('#markup' => l('Create new nodequeue container', 'admin/structure/nodequeue/containers/add'));
	}

	// Hidden field to hold our cid
	$form['cid'] = array(
		'#type' => 'hidden',
		'#default_value' => $row->cid,
	);
	
	
	// Fetches layout field
	$form['layout'] = _nodequeue_containers_build_form_layout_field($row->layout);
  
  	
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
	);
  		
  return $form;
}

/**
 * Validate handler for nodequeue_containers_form_layout()
 */
function nodequeue_containers_form_layout_validate(&$form, &$state) {
	// Validates that a nodequeue container on given id
	if(!nodequeue_containers_load($state['values']['cid'])) {
		form_set_error('cid', t('Couldn\'t find nodequeue container.'));
	}
}

/**
 * Submit handler for nodequeue_containers_form_layout()
 */
function nodequeue_containers_form_layout_submit(&$form, &$state) {
	$container = nodequeue_containers_load($state['values']['cid']);
	
	// Saves changes
	$sth = db_update('nodequeue_containers')
				 ->condition('cid', $container->cid, '=')
				 ->fields(array(
				 	'layout' => $state['values']['layout'],
				 ))
				 ->execute();
	
	
	drupal_set_message(t('The node container @name is now saved.', array('@name' => $container->name)));
	drupal_goto('admin/structure/nodequeue/'. $container->qid .'/view/'. $container->sqid);
}


/**
 * Page providing autocomplete values for nodequeue selection in containers add/edit form
 */
function nodequeue_containers_nodequeue_autocomplete($q) {
	$matches = array();


	// Finds all nodequeue queues that matches our search
	$result = db_select('nodequeue_subqueue', 's')
						->fields('s', array('sqid','title'))
						->condition('s.title', '%' . db_like($q) . '%', 'LIKE')
						->execute();
	// And build matches array
	foreach ($result as $row):
		$matches[$row->sqid .'-'. $row->title] = check_plain($row->title);
	endforeach;
	
	drupal_json_output($matches);
}