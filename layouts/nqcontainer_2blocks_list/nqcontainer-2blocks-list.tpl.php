<?php
/**
 * @file
 * Template for mtcontainers layout
 *
 * Variables:
 * - $positions: An array of content for each positions (numeric, position 1 should use $positions[1] etc.
 * - $container An array of settings for the container currently using this theme
 */
?>
<div class="node-container nqcontainer_4blocks_list">
		
	<!-- Top cols left -->
	<div class="nqcontainer_4blocks_list-left-top left position-1">
		<?php print $positions[1]; ?>
	</div>
	
	<div class="nqcontainer_4blocks_list-left-top right position-2">
		<?php print $positions[2]; ?>
	</div>
	
	<div style="clear:both; height:10px;">&shy;</div>
	
	
	<!-- Bottom cols left -->
	<div class="nqcontainer_4blocks_list-left-bottom positions-3-6">
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-3">
			<?php print $positions[3]; ?>
		</div>
		
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-4">
			<?php print $positions[4]; ?>
		</div>
		
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-5">
			<?php print $positions[5]; ?>
		</div>
		
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-6">
			<?php print $positions[6]; ?>
		</div>
	</div>
	
</div>