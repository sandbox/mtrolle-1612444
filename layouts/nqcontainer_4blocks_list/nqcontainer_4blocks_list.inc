<?php

// Plugin definition
$plugin = array(
  'title' => t('4 blocks and a list'),
  'category' => '3 cols',
  'icon' => 'nqcontainer_4blocks_list.png',
  'theme' => 'nqcontainer_4blocks_list',
  'css' => 'nqcontainer_4blocks_list.css',
  'positions' => array(
    1 => array(), // its safe to leave an empty array as this will default to all available fields
    2 => array(), // its safe to leave an empty array as this will default to all available fields
    3 => array(), // its safe to leave an empty array as this will default to all available fields
    4 => array(), // its safe to leave an empty array as this will default to all available fields
    5 => array('title', 'teaser'), // we only want teaser and titles here
    6 => array('title', 'teaser'), // we only want teaser and titles here
    7 => array('title', 'teaser'), // we only want title here
    8 => array('title', 'teaser'), // we only want title here
  ),
);
