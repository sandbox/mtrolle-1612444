<?php

// Plugin definition
$plugin = array(
  'title' => t('1 large, 3 cols + list'),
  'category' => '3 cols',
  'icon' => 'nqcontainer_1large_3cols_list.png',
  'theme' => 'nqcontainer_1large_3cols_list',
  'css' => 'nqcontainer_1large_3cols_list.css',
  'positions' => array(
    1 => array('image-large', 'title', 'teaser', 'related'), // its safe to leave an empty array as this will default to all available fields
    2 => array(), // its safe to leave an empty array as this will default to all available fields
    3 => array(), // its safe to leave an empty array as this will default to all available fields
    4 => array(), // its safe to leave an empty array as this will default to all available fields
    5 => array('title', 'teaser'), // we only want title here
    6 => array('title'), // we only want title here
    7 => array('title'), // we only want title here
    8 => array('title'), // we only want title here
    9 => array('title'), // we only want title here
  ),
);
