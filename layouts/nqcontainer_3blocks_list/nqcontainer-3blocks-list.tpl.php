<?php
/**
 * @file
 * Template for nqcontainers layout
 *
 * Variables:
 * - $positions: An array of content for each positions (numeric, position 1 should use $positions[1] etc.
 * - $container An array of settings for the container currently using this theme
 */
?>
<div class="node-container nqcontainer-3blocks-list">

	<div class="nqcontainer-3blocks-list-block left position-1">
		<?php print $positions[1]; ?>
	</div>
	
	<div class="nqcontainer-3blocks-list-block right position-2">
		<?php print $positions[2]; ?>
	</div>
		
	<!-- Line break spacer -->
	<div style="clear:both; height:10px;">&shy;</div>
	
	<div class="nqcontainer-3blocks-list-block left position-3">
		<div class="nqcontainer-hr">&nbsp;</div>
		<?php print $positions[3]; ?>
	</div>
	
	<div class="nqcontainer-3blocks-list-block right positions-4-8">
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-4">
			<?php print $positions[4]; ?>
		</div>
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-5">
			<?php print $positions[5]; ?>
		</div>
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-6">
			<?php print $positions[6]; ?>
		</div>
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-7">
			<?php print $positions[7]; ?>
		</div>
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-8">
			<?php print $positions[8]; ?>
		</div>
	</div>
</div>