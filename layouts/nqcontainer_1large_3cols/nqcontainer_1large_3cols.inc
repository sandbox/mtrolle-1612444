<?php

// Plugin definition
$plugin = array(
  'title' => t('1 large & 3 blocks'),
  'category' => '3 cols',
  'icon' => 'nqcontainer_1large_3cols.png',
  'theme' => 'nqcontainer_1large_3cols',
  'css' => 'nqcontainer_1large_3cols.css',
  'positions' => array(
    1 => array('image-large', 'title', 'teaser', 'related'), // its safe to leave an empty array as this will default to all available fields
    2 => array(), // its safe to leave an empty array as this will default to all available fields
    3 => array(), // its safe to leave an empty array as this will default to all available fields
    4 => array(), // its safe to leave an empty array as this will default to all available fields
  ),
);
