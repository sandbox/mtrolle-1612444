<?php
/**
 * @file
 * Template for nqcontainers layout
 *
 * Variables:
 * - $positions: An array of content for each positions (numeric, position 1 should use $positions[1] etc.
 * - $container An array of settings for the container currently using this theme
 */
?>
<div class="node-container nqcontainer_1block_list">

	<div class="nqcontainer_1block_list-block position-1">
		<?php print $positions[1]; ?>
	</div>
	
	<div class="nqcontainer_1block_list-list positions-2-to-4">
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-2">
			<?php print $positions[2]; ?>
		</div>
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-3">
			<?php print $positions[3]; ?>
		</div>
		<div class="nqcontainer-hr">&nbsp;</div>
		<div class="position-4">
			<?php print $positions[4]; ?>
		</div>
	</div>
</div>