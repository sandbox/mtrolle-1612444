<?php

// Plugin definition
$plugin = array(
  'title' => t('2 large, 7 blocks & list'),
  'category' => '3 cols',
  'icon' => 'nqcontainer_2large_7blocks_list.png',
  'theme' => 'nqcontainer_2large_7blocks_list',
  'css' => 'nqcontainer_2large_7blocks_list.css',
  'positions' => array(
    1 => array('image-large', 'title', 'teaser', 'related'), // its safe to leave an empty array as this will default to all available fields
    2 => array(), // its safe to leave an empty array as this will default to all available fields
    3 => array('image', 'title'), // its safe to leave an empty array as this will default to all available fields
    4 => array('image', 'title'), // its safe to leave an empty array as this will default to all available fields
    5 => array('image', 'title'), // its safe to leave an empty array as this will default to all available fields
    6 => array('image', 'title'), // its safe to leave an empty array as this will default to all available fields
    7 => array('image', 'title'), // its safe to leave an empty array as this will default to all available fields
    8 => array('image', 'title'), // its safe to leave an empty array as this will default to all available fields
    9 => array('image', 'title'), // its safe to leave an empty array as this will default to all available fields
    10 => array('title'), // its safe to leave an empty array as this will default to all available fields
    11 => array('title'), // its safe to leave an empty array as this will default to all available fields
    12 => array('title'), // its safe to leave an empty array as this will default to all available fields
    13 => array('title'), // its safe to leave an empty array as this will default to all available fields
    14 => array('title'), // its safe to leave an empty array as this will default to all available fields
    15 => array('title'), // its safe to leave an empty array as this will default to all available fields
  ),
);
