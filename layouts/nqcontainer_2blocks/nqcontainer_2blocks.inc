<?php

// Plugin definition
$plugin = array(
  'title' => t('2 blocks'),
  'category' => '2 cols',
  'icon' => 'nqcontainer_2blocks.png',
  'theme' => 'nqcontainer_2blocks',
  'css' => 'nqcontainer_2blocks.css',
  'positions' => array(
    1 => array(), // its safe to leave an empty array as this will default to all available fields
    2 => array(), // its safe to leave an empty array as this will default to all available fields
  ),
);
