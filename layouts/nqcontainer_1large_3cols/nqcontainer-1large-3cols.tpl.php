<?php
/**
 * @file
 * Template for nqcontainers layout
 *
 * Variables:
 * - $positions: An array of content for each positions (numeric, position 1 should use $positions[1] etc.
 * - $container An array of settings for the container currently using this theme
 */
?>
<div class="node-container nqcontainer_1large_3cols">

	<div class="nqcontainer_1large_3cols-block-top position-1">
		<?php print $positions[1]; ?>
	</div>
	
	<!-- Line break spacer -->
	<div style="clear:both; height:10px;">&shy;</div>
	
	<div class="nqcontainer_1large_3cols-block-bottom left position-2">
		<div class="nqcontainer-hr">&nbsp;</div>
		<?php print $positions[2]; ?>
	</div>
	<div class="nqcontainer_1large_3cols-block-bottom center position-3">
		<div class="nqcontainer-hr">&nbsp;</div>
		<?php print $positions[3]; ?>
	</div>
	<div class="nqcontainer_1large_3cols-block-bottom right position-4">
		<div class="nqcontainer-hr">&nbsp;</div>
		<?php print $positions[4]; ?>
	</div>


	<div style="clear:both; height:10px;">&shy;</div>
</div>